"use strict";

exports.scrape = function (casper, func) {
  casper.evaluate(function (func) {
    // Avoid collisions
    var jq214 = jQuery.noConflict(true);

    // Helper Module
    var Scraper =
      (function (document, window, $, undefined) {
        var deferred = $.Deferred(),
          data = {
            status: 'init',
            data: {},
            links: {}
          };

        // Closures
        return {
          init: function () {
            return deferred;
          },

          links: function (sel, type) {
            if (!Array.isArray(data.links[type])) {
              data.links[type] = [];
            }

            $(sel).map(function () {
              if (this.href !== undefined &&
                // Ignore duplicates
                data.links[type].indexOf(this.href) === -1) {
                data.links[type].push(this.href);
              }
            }).get();
          },

          action: function (event, sel, i, fn) {
            var self = this,
              elems = $(sel),
              elem = elems.get(i),
              handler = $.Deferred();

            switch (event) {
            case 'click':
              handler = $(elem).triggerHandler('click');
              break;
            case 'select':
              handler = $(elem).prop('selected', true).change();
              break;
            }

            $.when(handler)
              .done(function () {
                fn(elems.get(i));

                i += 1;
                if (i < elems.length) {
                  self.action(event, sel, i, fn);
                }
              });
          },

          step: function (cond, action, sel, next) {
            if (cond) {
              Scraper.action(action, sel, 0, next);
            } else {
              next();
            }
          },

          add: function (id, val) {
            data.data[id] = val;
          },

          get: function (id) {
            return data.data[id];
          },

          sum: function (arr) {
            return arr.reduce(function (a, b) {
              return a + b;
            });
          },

          avg: function (arr) {
            return arr.length > 0 ? this.sum(arr) / arr.length : 0;
          },

          screenshot: function (name) {
            if (!name) {
              var d = new Date();
              name = d.getTime();
            }

            window.callPhantom({
              action: 'screenshot',
              name: name
            });
          },

          err: function (err, elem) {
            console.log(err, elem);

            data.status = 'error';
            this.add('_msg', {
              err: err,
              elem: elem
            });

            // Abort on error
            deferred.resolve();
          },

          finish: function () {
            data.status = 'complete';
            deferred.resolve();
          },

          // Called by $.when() to really finish
          exit: function () {
            window.callPhantom(data);
          }
        };

      })(document, window, jq214);

    // Quit when it's done
    jq214.when(Scraper.init()).done(function () {
      Scraper.exit();
    });

    // Run the site function on page
    try {
      jq214(document).ready(function () {
        eval(func);
      });
    } catch (msg) {
      Scraper.err(msg);
    }
  }, func);
};
