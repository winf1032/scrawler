"use strict";

var cp = require('child_process');
var server = cp.fork(__dirname + '/server/app.js');

server.on('message', function (res) {
  if (res.state === 'success') {
    // Ready to rumble
    console.log('server started: ', res.url);
  } else {
    console.err('server error: ', res.msg);
  }
});
