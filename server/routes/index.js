"use strict";

var express = require('express');
var router = express.Router();
var siteView = require('./control/siteView');
var scrapeView = require('./control/scrapeView');

/* GET home page. */
router.get('/', function (req, res) {
  res.redirect('/index.html');
});

router.get('/sites.html', function (req, res) {
  siteView.index(req, res);
});

router.get('/sites/:id.html', function (req, res) {
  siteView.detail(req, res);
});

router.get('/scrape.html', function (req, res) {
  scrapeView.index(req, res);
});

router.get('/:page.html', function (req, res) {
  res.render(req.params.page);
});


router.get('/:dir/:page.html', function (req, res) {
  res.render(req.param('dir') + '/' + req.param('page')); //.replace(".html", ""));
});

module.exports = router;
