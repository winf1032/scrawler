"use strict";

var _ = require('underscore');
var Q = require('q');
var express = require('express');
var router = express.Router();
var siteDb = require('../../../lib/db/siteDb');
var x = require('../../../lib/helper');

// Get selection data for the export
router.get('/export/', function (req, res, next) {
  var promises = [Q.promise.resolve()],
    data = {};

  promises.push(siteDb.getDistinct('type', {})
    .then(function (res) {
      data.type = res;
    }));

  // Waiting for it...
  return Q.all(promises).then(function () {
    res.json(data);
  }, function (err) {
    x.err(res, err);
  });
});

router.get('/get/', function (req, res, next) {
  siteDb.getSites(null)
    .then(function (data) {
      res.json(data);
    }, function (err) {
      x.err(res, err);
    });
});

router.get('/get/:domain', function (req, res, next) {
  var domain = req.params.domain;

  siteDb.getSites(domain)
    .then(function (data) {
      res.json(data);
    }, function (err) {
      x.err(res, err);
    });
});

router.get('/get/:domain/:type', function (req, res, next) {
  var domain = req.params.domain,
    type = req.params.type;

  siteDb.getSite(domain, type)
    .then(function (data) {
      res.json(data);
    }, function (err) {
      x.err(res, err);
    });
});

// Add new site
router.post('/add/', function (req, res, next) {
  var site = JSON.parse(req.body.data);

  // Only accept correct sites
  if (
    _.has(site, 'domain') &&
    _.has(site, 'type') &&
    _.has(site, 'function') &&
    _.has(site, 'timeout')
  ) {
    siteDb.addSite(site)
      .then(function (data) {
        res.json(data);
      }, function (err) {
        x.err(res, err);
      });
  } else {
    x.err(res, 'wrong parameters');
  }

});

// Delete site receipt
router.post('/delete/', function (req, res, next) {
  var id = x.jsonParse(req.body.data);

  if (id) {
    siteDb.deleteSite(id)
      .then(function (data) {
        res.json(data);
      }, function (err) {
        x.err(res, err);
      });
  } else {
    x.err(res, 'wrong parameters');
  }

});

module.exports = router;
