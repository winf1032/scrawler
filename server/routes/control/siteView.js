"use strict";

var _ = require('underscore');
var Q = require('q');
var express = require('express');
var router = express.Router();
var siteDb = require('../../../lib/db/siteDb');
var x = require('../../../lib/helper');



exports.index = function (req, res) {
  siteDb.getSites(null)
    .then(function (sites) {
      res.render('sites', {
        domain: '',
        type: '',
        timeout: 15,
        func: '',
        sites: sites
      });
    }, function (err) {
      x.err(res, err);
    });
};

exports.detail = function (req, res) {
  var id = req.params.id;

  siteDb.getSites(null)
    .then(function (sites) {
      var sel = _.find(sites, function (site) {
        return site._id == id;
      });

      if (sel) {
        res.render('sites', {
          id: sel._id,
          domain: sel.domain,
          type: sel.type,
          timeout: sel.timeout / 1000,
          func: sel.function,
          sites: sites
        });
      } else {
        res.render('404');
      }
    }, function (err) {
      x.err(res, err);
    });
};
