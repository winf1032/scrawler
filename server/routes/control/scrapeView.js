"use strict";

var _ = require('underscore');
var Q = require('q');
var express = require('express');
var router = express.Router();
var siteDb = require('../../../lib/db/siteDb');
var x = require('../../../lib/helper');



exports.index = function (req, res) {
  siteDb.getSites(null)
    .then(function (sites) {
      res.render('scrape', {
        depth: 100,
        idleMin: 700,
        idleMax: 3000,
        api: 'http://localhost:3000/api/',
        verbose: true,
        logLevels: ['debug', 'info', 'warning', 'error'],
        logLevel: 'warning',
        loadImages: false,
        loadPlugins: false,
        vpWidth: 1280,
        vpHeight: 1024,
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/1337.0.2454.99 Safari/537.36',
      });
    }, function (err) {
      x.err(res, err);
    });
};
