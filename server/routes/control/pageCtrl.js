"use strict";

var _ = require('underscore');
var Q = require('q');
var URI = require('URIjs');
var express = require('express');
var router = express.Router();
var siteDb = require('../../../lib/db/siteDb');
var pageDb = require('../../../lib/db/pageDb');
var dataDb = require('../../../lib/db/dataDb');
var Page = require('../../../lib/page');
var x = require('../../../lib/helper');


// Get selection data for the export
router.get('/export/', function (req, res, next) {
  var promises = [Q.promise.resolve()],
    data = {};

  promises.push(pageDb.getDistinct('type', {})
    .then(function (res) {
      data.type = res;
    }));

  promises.push(pageDb.getDistinct('tag', {})
    .then(function (res) {
      data.tags = res;
    }));

  promises.push(pageDb.getDistinct('status', {})
    .then(function (res) {
      data.status = res;
    }));


  // Waiting for it...
  return Q.all(promises).then(function () {
    res.json(data);
  }, function (err) {
    x.err(res, err);
  });
});

// Export pages including data
router.get('/get/all/', function (req, res, next) {
  var filter = req.query.filter,
    download = req.query.download,
    denormalize = req.query.denormalize;

  pageDb.getPages(true, filter)
    .then(function (pages) {
        if (pages.length > 0) {
          var data = [];
          var json2csv = require('json2csv');
          var fields = ['url', 'status', 'count', 'depth', 'type', 'tag'];
          var fieldNames = ['URL', 'Status', 'Count', 'Depth', 'Type', 'Tag'];

          /*
              denormalize = {
                name: 'data',
                field: 'articles'
              };
              */

          if (pages[0].data.length > 0) {
            // Denormalize an array (multiple single values per page)
            if (_.isObject(denormalize) &&
              _.isArray(pages[0].data[0].data[denormalize.field])) {
              _.each(pages, function (page) {
                // Append the whole page data
                var rows =
                  _.map(page.data[0].data[denormalize.field], function (nested) {
                    var row = {};
                    row[denormalize.name] = nested;

                    // Add duplicate basic fields
                    _.each(fields, function (field) {
                      row[field] = page[field];
                    });

                    return row;
                  });

                data = _.union(data, rows);
              });

              // Get keys from the first data object
              _.each(_.keys(data[0][denormalize.name]), function (key, i) {
                fields.push(denormalize.name + '.' + key);
                fieldNames.push('Data ' + key.charAt(0).toUpperCase() + key.slice(1));
              });
            } else {
              data = pages;

              // Get keys from the first data object
              _.each(_.keys(pages[0].data[0].data), function (key, i) {
                fields.push('data[0].data.' + key);
                fieldNames.push('Data ' + key.charAt(0).toUpperCase() + key.slice(1));
              });
            }
          } else {
            // No data yet, display jsut the page object
            data = pages;
          }

          var opts = {
            data: data,
            fields: fields,
            fieldNames: fieldNames,
            nested: true,
            del: ';',
            newLine: "\r\n"
          };

          if (download) {
            json2csv(opts, function (err, csv) {
              if (!err) {
                var filename = 'export';
                if (filter.type) {
                  filename += ' ' + _.isArray(filter.type) ?
                    filter.type.join(' ') : filter.type;
                }
                if (filter.tag) {
                  filename += ' ' + _.isArray(filter.tag) ?
                    filter.type.tag(' ') : filter.tag;
                }
                if (filter.status) {
                  filename += ' ' + _.isArray(filter.status) ?
                    filter.type.status(' ') : filter.status;
                }

                res.setHeader('Content-Type', 'text/csv');
                res.setHeader('Content-Disposition', 'attachment; filename=' + filename + '.csv;');
                res.end(csv, 'binary');
              } else {
                x.err(res, err);
              }
            });
          } else {
            res.send(data);
          }
        } else {
          res.send([]);
        }
      },
      function (err) {
        x.err(res, err);
      });

});

// Get next URL to operate on
router.get('/get/:id', function (req, res, next) {
  var id = req.params.id,
    populate = req.query.populate;

  pageDb.getPage({
    _id: id
  }, (populate == 1)).then(function (data) {
    res.json(data);
  }, function (err) {
    x.err(res, err);
  });
});

router.get('/count/', function (req, res, next) {
  pageDb.getCount()
    .then(function (data) {
      res.json(data);
    }, function (err) {
      x.err(res, err);
    });
});

// Block a page
router.get('/block/:id', function (req, res, next) {
  var id = req.params.id;

  pageDb.getPage({
    _id: id
  }, false).then(function (data) {
    // Check if page is already blocked
    if (data.status !== 2) {
      pageDb.updatePage(id, {
        status: 2
      }).then(function (page) {
        siteDb.getSite(Page.getDomain(page.url), page.type)
          .then(function (site) {
            res.json(site);
          }, function (err) {
            x.err(res, err);
          });
      }, function (err) {
        x.err(res, err);
      });
    } else {
      x.err(res, 'Page is blocked');
    }
  }, function (err) {
    x.err(res, err);
  });
});

// Get next URL to operate on
router.post('/next/', function (req, res, next) {
  var page = _.has(req.body, 'page') ? x.jsonParse(req.body.page) : {};

  pageDb.getNextPage(page)
    .then(function (data) {
      res.json(data);
    }, function (err) {
      x.err(res, err);
    });
});

// Change a page status
router.get('/status/:id/:status', function (req, res, next) {
  var id = req.params.id,
    status = req.params.status;

  pageDb.updatePage(id, {
    status: status
  }).then(function (page) {
    //TODO switch mit case 3 data return
    // Return site data if page got blocked
    if (status == 2) {
      siteDb.getSite(Page.getDomain(page.url), page.type)
        .then(function (site) {
          res.json(site);
        }, function (err) {
          x.err(res, err);
        });
    } else {
      // Return regular page information
      res.json(page);
    }
  }, function (err) {
    x.err(res, err);
  });
});

// Add new pages
router.post('/add/', function (req, res, next) {
  var self = this,
    data = x.jsonParse(req.body.data),
    pages = [],
    urls = data.urls,
    type = data.type,
    tag = data.tag;

  if (Array.isArray(urls) && tag) {
    for (var i = 0; i < urls.length; i++) {
      pages.push({
        url: urls[i],
        status: 1, // Override status if manual added
        type: type,
        tag: tag
      });
    }

    if (pages.length > 0) {
      Page.savePages(pages).then(function (data) {
        res.json(data);
      }, function (err) {
        x.err(res, err);
      });
    } else {
      x.err(res, 'No valid url');
    }
  } else {
    x.err(res, 'Wrong Parameter');
  }
});

// Reset pages
router.post('/reset/', function (req, res, next) {
  var self = this,
    data = x.jsonParse(req.body.data),
    filter = data.filter;

  pageDb.update(filter, {
    status: 1
  }).then(function (page) {
    res.json(page);
  }, function (err) {
    x.err(res, err);
  });
});

// Reset pages
router.post('/delete/', function (req, res, next) {
  var self = this,
    data = x.jsonParse(req.body.data),
    filter = data.filter;

  pageDb.delete(filter).then(function (page) {
    res.json(page);
  }, function (err) {
    x.err(res, err);
  });
});

// Finish a page
router.post('/finish/:id', function (req, res, next) {
  var id = req.params.id,
    promises = [Q.promise.resolve()],
    status = _.has(req.body, 'status') ? x.jsonParse(req.body.status) : null,
    data = _.has(req.body, 'data') ? x.jsonParse(req.body.data) : {},
    pages = _.has(req.body, 'pages') ? x.jsonParse(req.body.pages) : [];

  // Adding pages
  if (pages.length > 0) {
    var deferred_pages = Q.defer();

    Page.savePages(pages)
      .then(function (res) {
        deferred_pages.resolve(res);
      }, function (err) {
        deferred_pages.reject(new Error(err));
      });

    promises.push(deferred_pages.promise);
  }

  // Adding data
  if (!_.isEmpty(data)) {
    var deferred_data = Q.defer();

    Page.saveData(id, data)
      .then(function (res) {
        deferred_data.resolve(res);
      }, function (err) {
        deferred_data.reject(new Error(err));
      });

    promises.push(deferred_data.promise);
  }

  // Waiting for pages & data to be added
  Q.all(promises).then(function () {
    // Save pages status
    status = Page.getStatus(status);

    pageDb.updatePage(id, {
      status: status
    }).then(function (page) {
      res.json(page);
    }, function (err) {
      x.err(res, err);
    });
  }, function (err) {
    x.err(res, err);
  });

});
module.exports = router;
