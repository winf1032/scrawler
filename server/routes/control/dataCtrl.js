"use strict";

var dbData = require('../../../lib/db/dataDb');
var express = require('express');
var router = express.Router();
var x = require('../../../lib/helper');

// Get data linked to a page
router.get('/get/page/:id', function (req, res, next) {
  var id = req.params.id,
    populate = req.query.populate;

  dbData.getData({
    page: id
  }, (populate == 1)).then(function (data) {
    res.json(data);
  }, function (err) {
    x.err(res, err);
  });
});

// Add some scraped data
router.post('/add/:id', function (req, res, next) {
  var id = req.params.id,
    data = x.jsonParse(req.body.data);

  dbData.addData(id, data).then(function (data) {
    res.json(data);
  }, function (err) {
    x.err(res, err);
  });
});

module.exports = router;
