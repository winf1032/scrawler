"use strict";

var express = require('express'),
  bodyParser = require('body-parser'),
  morgan = require('morgan'),
  http = require('http'),
  path = require('path');

var db = require('../lib/db/initDb');

var routes = require('./routes/index');
var siteCtrl = require('./routes/control/siteCtrl');
var pageCtrl = require('./routes/control/pageCtrl');
var dataCtrl = require('./routes/control/dataCtrl');
var app = module.exports = express();

app.set('port', process.env.PORT || 3000);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true // Support encoded bodies
}));

var env = process.env.NODE_ENV || 'development';

if (env === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use('/', routes);
app.use('/api/site', siteCtrl);
app.use('/api/page', pageCtrl);
app.use('/api/data', dataCtrl);

// redirect all others to the index (HTML5 history)
app.get('*', function (req, res, next) {
  res.render('index');
});

var server = http.createServer(app);
var io = require('./lib/sockets').listen(server);

server.listen(app.get('port'), function (err) {
  var address = server.address(),
    url = address.family === 'IPv6' ? '[' + address.address + ']' : address.address;

  process.send({
    state: 'success',
    url: 'http://' + url + ':' + app.get('port')
  });
});
