"use strict";

(function (window, document, $, undefined) {
  $.get("/api/site/export/", function (data) {
    // For placeholder value
    data.type.unshift('');

    $('#pageType').select2({
      data: data.type
    });
  }, "json");

  // Add page
  $("#addPage #btnAdd").click(function () {
    function textToArray(text) {
      var pages = [];

      $.each(text.split(/\n/), function (i, url) {
        if (url !== '') {
          pages.push(url);
        }
      });

      return pages;
    }

    var urls = textToArray($("#multPages").val()),
      url = $('#pageUrl').val(),
      type = $('#pageType').val(),
      tag = $('#pageTag').val();

    if (url) {
      urls.unshift(url);
    }

    if ((urls.length > 0) && type && tag) {
      var jqxhr = $.post('/api/page/add/', {
          data: JSON.stringify({
            urls: urls,
            type: type,
            tag: tag
          })
        })
        .done(function (data) {
          document.location.reload(true);
        })
        .fail(function (err) {
          console.log(err);
        })
        .always(function () {});
    }
  });

})(window, document, $);
