"use strict";

(function (window, document, $, undefined) {
  // Get the filter settings
  function getVals() {
    return {
      type: $('#pageType').val(),
      tag: $('#pageTag').val(),
      status: $('#pageStatus').val(),
      url: $('#pageUrl').val(),
    };
  }

  // Get all existing page information
  $.get("/api/page/export/", function (data) {
    var status = [];
    var statusNames = [
      '', 'Pending', 'Blocked', 'Timeout', 'Error', 'Finished'
    ];

    // Combine status types with names
    for (var i = 5; i > 0; i--) {
      if (data.status.indexOf(i) !== -1) {
        status.push({
          id: i,
          text: statusNames[i]
        });
      }
    }

    $('#pageType').select2({
      data: data.type
    });
    $('#pageTag').select2({
      data: data.tags
    });
    $('#pageStatus').select2({
      data: status
    });
  }, "json");


  $("#btnPreview").click(function () {
    var url = '/api/page/get/all';
    url += '?' + $.param({
      filter: getVals()
    });

    $.get(url)
      .done(function (data) {
        $('#preview pre').html(JSON.stringify(data, undefined, 2));
        $('#preview h3 small').html(' ' + data.length + ' entries in total');
        $('#preview').show();
      })
      .fail(function (err) {
        console.log(err);
      })
      .always(function () {});
  });

  // Button Download
  $('#btnDownload').click(function (e) {
    var url = '/api/page/get/all';
    url += '?' + $.param({
      filter: getVals(),
      download: 1
    });

    e.preventDefault(); //stop the browser from following
    window.location.href = url;
  });

  $("#confirm .btn-ok").click(function () {
    var link = $('#confirm .btn-ok').attr('href');

    var jqxhr = $.post(link, {
        data: JSON.stringify({
          filter: getVals()
        })
      })
      .done(function (data) {
        document.location.reload(true);
      })
      .fail(function (err) {
        console.log(err);
      })
      .always(function () {});

    return false;
  });

})(window, document, $);
