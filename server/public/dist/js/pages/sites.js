"use strict";

(function (window, document, $, undefined) {
  // Add site
  $('#saveSite').click(function () {
    var site = {
      domain: $('.addSite #siteDomain').val(),
      type: $('.addSite #siteType').val(),
      timeout: $('.addSite #siteTimeout').val(),
      function: $('.addSite #siteFunction').val()
    };

    //TODO useful condition
    if (true) {
      var jqxhr = $.post('/api/site/add/', {
          data: JSON.stringify(site)
        })
        .done(function (data) {
          document.location.href = '/sites/' + data._id + '.html';
        })
        .fail(function (err) {
          console.log(err);
        })
        .always(function () {});
    }
  });


  $("#confirm .btn-ok").click(function () {
    var id = $('#deleteSite').attr('data-id');
    var jqxhr = $.post('/api/site/delete/', {
        data: JSON.stringify(id)
      })
      .done(function (data) {
        document.location.href = '/sites.html';
      })
      .fail(function (err) {
        console.log(err);
      })
      .always(function () {});
  });
})(window, document, $);
