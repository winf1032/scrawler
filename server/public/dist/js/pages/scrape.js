"use strict";

(function (window, document, $, io, undefined) {
  // Get the filter settings
  function getVals() {
    return {
      url: $('#scrapeUrl').val(),
      type: $('#scrapeType').val(),
      tag: $('#scrapeTag').val(),
      depth: $('#scrapeDepth').val(),
      idleMin: $('#scrapeIdleMin').val(),
      idleMax: $('#scrapeIdleMax').val(),
      api: $('#scrapeApi').val(),
      verbose: $('#scrapeVerbose').is(':checked'),
      logLevel: $('#scrapeLogLevel').val(),
      plugins: $('#scrapePlugins').is(':checked'),
      images: $('#scrapeImages').is(':checked'),
      width: $('#scrapeWidth').val(),
      height: $('#scrapeHeight').val(),
      userAgent: $('#scrapeUserAgent').val()
    };
  }

  // Get all existing page information
  $.get("/api/page/export/", function (data) {
    // For placeholder values
    data.type.unshift('');
    data.tags.unshift('');

    $('#scrapeType').select2({
      data: data.type
    });
    $('#scrapeTag').select2({
      data: data.tags
    });
  }, "json");



  // Enable scraping when connected
  io.on('connection', function (id) {
    $("#scrapeStart").click(function () {
      io.emit('scrapeStart', getVals());
    });

    io.on('spawned', function (data) {
      var tpl = $('#template #console')
        .clone()
        .attr('id', 'console_' + data.id)
        .appendTo('#scraper');

      $(tpl).find('small').html(' ' + data.name);
      $(tpl).find('.scrapeStop').click(function () {
        io.emit('scrapeStop', data.id);
      });

    });

    // Console output
    io.on('msg', function (data) {
      $('#console_' + data.id + ' .output')
        .append('$ ' + data.msg);
    });

    // Refresh badges
    io.on('finished', function (data) {
      var badge = $('#console_' + data.id + ' .box-header .' + data.type);
      $(badge).html(parseInt($(badge).html()) + 1);
    });
  });

})(window, document, $, io(location.host + '/scrape'));
