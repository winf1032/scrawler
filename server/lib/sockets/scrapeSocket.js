"use strict";

var _ = require('underscore');

module.exports = function (io) {
  io.of('/scrape') // Scope
    .on('connection', function (socket) {
      // Hotbed
      var spawn = require('child_process').spawn;

      // Family tree
      var childs = [];

      // Stopping the program
      function kill(cp) {
        cp.kill('SIGHUP');
      }

      // Confirm connection
      socket.emit('connection');

      // Start the scraping process
      socket.on('scrapeStart', function (data) {
        // Basic arguments
        var id = childs.length,
          // Basic parameter
          args = ['casper.js', '--ssl-protocol=any'],
          // Frontend identifier
          names = ['url', 'type', 'tag'],
          filter = [];

        // Extended the arguments
        var keys = _.keys(data);
        _.each(keys, function (key) {
          if (_.has(data, key) && data[key]) {
            // Set the console name filter
            if (_.indexOf(names, key) !== -1) {
              filter.push(key + ': ' + data[key]);
            }

            // Set all important arguments
            args.push('--' + key + '=' + data[key] + '');
          }
        });

        // Spawn the child process
        var casper = spawn('casperjs', args);

        // Set all communication channels
        casper.stdout.setEncoding('utf8');

        // Default notification
        casper.stdout.on('data', function (msg) {
          // Status badge counter
          if (msg.indexOf('status: ') === 0) {
            var type = 'complete';
            if (msg.indexOf('timeout') > -1) {
              type = 'timeout';
            } else if (msg.indexOf('error') > -1) {
              type = 'error';
            }

            // Emit badge refresh
            socket.emit('finished', {
              id: id,
              type: type,
              msg: ''
            });
          }

          socket.emit('msg', {
            id: id,
            type: 'normal',
            msg: msg
          });
        });

        // Error notification
        casper.stderr.on('data', function (msg) {
          socket.emit('msg', {
            id: id,
            type: 'error',
            msg: msg
          });
        });

        // Exit notification
        casper.on('exit', function (code) {
          socket.emit('msg', {
            id: id,
            type: 'exit',
            msg: 'process exit (' + code + ')'
          });
        });

        // Shut down correctly
        socket.on('disconnect', function () {
          kill(casper);
        });

        // Add child to family
        childs.push(casper);
        socket.emit('spawned', {
          id: id,
          name: (filter.length === 0) ?
            'all' : filter.join(', ')
        });
      });

      // Stop the scraping process
      socket.on('scrapeStop', function (id) {
        kill(childs[id]);
      });
    });

  return io;
};
