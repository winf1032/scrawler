"use strict";

var socketio = require('socket.io');

module.exports.listen = function (app) {
  var io = socketio.listen(app);

  // Register the page sockets
  var scrapeSocket = require('./sockets/scrapeSocket')(io);

  return io;
};
