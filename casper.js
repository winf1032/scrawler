"use strict";

var _ = require('underscore');
var Q = require('q');
var casper = require('casper').create();
var evalPage = require('../includes/evalPage');

// Default values, initializing
var cli = casper.cli,
  _conf = {
    page: null,
    site: null,
    remote: {},
    api: cli.has('api') ? cli.get('api') : 'http://localhost:3000/api/',
    url: cli.has('url') ? cli.get('url') : null,
    type: cli.has('type') ? cli.get('type') : null,
    tag: cli.has('tag') ? cli.get('tag') : null,
    depth: cli.has('depth') ? cli.get('depth') : null,
    idle: {
      min: cli.has('idleMin') ? cli.get('idleMin') : 700,
      max: cli.has('idleMax') ? cli.get('idleMax') : 3000
    },
    lastId: null
  };

// Correct the times if necessary
if (_conf.idle.min > _conf.idle.max) {
  _conf.idle.max = _conf.idle.min;
}

// Override CasperJS Configuration
casper.options.verbose = cli.has('verbose') ? true : false;

// 'debug', 'info', 'warning', 'error'
casper.options.logLevel = cli.has('logLevel') ? cli.get('logLevel') : 'warning';

casper.options.pageSettings = {
  loadImages: cli.has('loadImages') ? true : false,
  loadPlugins: cli.has('loadPlugins') ? true : false
};

casper.options.viewportSize = {
  width: cli.has('vpWidth') ? cli.get('vpWidth') : 1280,
  height: cli.has('vpHeight') ? cli.get('vpHeight') : 1024
};

var userAgent = cli.has('userAgent') ? cli.get('userAgent') : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/1337.0.2454.99 Safari/537.36';
casper.userAgent(userAgent);

function is200(casper) {
  return casper.status().currentHTTPStatus === 200;
}

function getNextPage() {
  console.log('getNextPage');
  var deferred = Q.defer(),
    next = _conf.api + 'page/next/';

  casper.open(next, {
    method: 'post',
    data: {
      page: JSON.stringify({
        url: _conf.url,
        type: _conf.type,
        tag: _conf.tag,
        depth: _conf.depth,
      })
    },
    headers: {
      'Accept': 'application/json',
      'Content-type': 'application/x-www-form-urlencoded'
    }
  }).then(function () {
    if (is200(casper)) {
      if (this.getPageContent() !== 'null') {
        // Page object will be returned
        _conf.page = JSON.parse(this.getPageContent());
        console.log('url: ' + _conf.page.url);

        if (_conf.lastId !== _conf.page._id) {
          _conf.lastId = _conf.page._id;
          deferred.resolve(true);
        } else {
          deferred.reject(new Error('Duplicate fail'));
        }
      } else {
        // No new page, consider job as done
        deferred.reject(new Error('No next page'));
      }
    } else {
      deferred.reject(new Error('Error getting next page'));
    }
  });

  return deferred.promise;
}

function blockPage() {
  console.log('blockPage');
  var deferred = Q.defer();

  casper.open(_conf.api + 'page/block/' + _conf.page._id, {
    method: 'get',
    headers: {
      'Accept': 'application/json'
    }
  }).then(function () {
    if (is200(casper) && this.getPageContent() !== 'null') {
      _conf.site = JSON.parse(this.getPageContent());
      deferred.resolve(true);
    } else {
      deferred.reject(new Error('Page not blockable'));
    }
  });

  return deferred.promise;
}

function operatePage() {
  console.log('operatePage');
  _conf.remote = {
    finished: false,
    data: {}
  };

  var deferred = Q.defer(),
    data = {};

  casper.open(_conf.page.url);

  // Start operating on page when it's loaded
  casper.waitForSelector('body', function () {
    // Needed client scripts
    casper.page.injectJs('./includes/jquery-2.1.4.min.js');

    // Apply scraping function
    if (_conf.site.function) {
      evalPage.scrape(casper, _conf.site.function);
    }
  });

  // Keep it running while operating
  casper.waitFor(function check() {
    return _conf.remote.finished;
  }, function then() {
    data = _conf.remote.data;
    deferred.resolve(data);
  }, function timeout() {
    data.status = 'timeout';
    deferred.resolve(data);
  }, _conf.site.timeout);

  return deferred.promise;
}

function finishPage(res) {
  console.log('finishPage');
  var deferred = Q.defer(),
    pages = [];

  // Update pages from links
  if (_.keys(res.links).length > 0) {
    _.each(_.keys(res.links), function (type, key) {
      _.each(res.links[type], function (link, i) {
        pages.push({
          url: link,
          status: null,
          type: type,
          tag: _conf.page.tag,
          depth: _conf.page.depth + 1
        });
      });
    });
  }

  // Debug info
  if (pages.length > 0) console.log('links: ' + pages.length);
  //console.log('data: ' + JSON.stringify(res.data));
  console.log('status: ' + res.status);

  // Debug screenshot
  if ((res.status != 'complete') ||  res.data.screenshot) {
    var d = new Date();
    var n = d.getTime();
    casper.capture('screenshots/fail/' + n + '.png');
  }

  casper.open(_conf.api + 'page/finish/' + _conf.page._id, {
    method: 'post',
    data: {
      status: JSON.stringify(res.status),
      data: JSON.stringify(res.data),
      pages: JSON.stringify(pages)
    },
    headers: {
      'Content-type': 'application/x-www-form-urlencoded'
    }
  }).then(function () {
    if (is200(casper)) {
      console.log('done.\n------------------------------------');
      deferred.resolve(true);
    } else {
      deferred.reject(
        new Error('Operating problem: finishPage()')
      );
    }
  });

  return deferred.promise;
}

function loop() {
  // Idle time for waiting
  var idle = _.random(_conf.idle.min, _conf.idle.max);

  _conf.page = null;
  process()
    .catch(function (err) {
      console.log(err);
      idle = _.random(_conf.idle.min * 3, _conf.idle.max * 3);
    }).fin(function () {
      console.log('Next loop in: ' + idle / 1000 + 's');
      casper.wait(idle, function () {
        // ...keep it going
        loop();
      });
    });
}

function process() {
  var deferred = Q.defer();
  getNextPage()
    .then(function (status) {
      if (status === null) {
        // No new page
        deferred.resolve(null);
      } else {
        // Do the hustle
        blockPage()
          .then(function () {
            return operatePage();
          })
          .then(function (data) {
            return finishPage(data);
          })
          .then(function (data) {
            deferred.resolve(true);
          })
          .catch(function (err) {
            deferred.reject(err);
          });
      }
    })
    .catch(function (err) {
      deferred.reject(err);
    });

  return deferred.promise;
}

casper.on('remote.message', function (msg) {
  //console.log(msg);
});

// Listen on remote Phantom call
casper.on('remote.callback', function (res) {
  if (res.action === 'screenshot') {
    casper.capture('screenshots/live/' + res.name + '.png');
  } else {
    // Global wormhole
    _conf.remote = {
      finished: true,
      data: res
    };
  }
});

casper.start();
casper.run(loop());
