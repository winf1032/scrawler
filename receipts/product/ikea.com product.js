/*global Scraper*/
"use strict";

// http://www.ikea.com/de/de/catalog/products/30222523/
// contain /catalog/products/

var prices = [],
  skus = 0,
  instock = 0,
  sizes = [
    '.displayMeasurements'
  ];

function getSku() {
  var size = 1,
    price = jq214('#price1').text().trim().toString();

  // For prices higher than 1000, remove first dot
  if ((price.match(/\./g) || []).length === 2) {
    price = price.replace('.', '');
  }

  price = parseFloat(price);

  var sizes = [
    '.displayMeasurements',
    '#subDivDropDown1 option[selected]'
  ];
  for (var i = 0; i < sizes.length; i++) {
    if (jq214(sizes[i]).length) {
      size = jq214(sizes[i]).text();
      break;
    }
  }

  if (size === null) {
    //Scraper.err('No size selector');
  }

  skus += 1;
  instock = instock + $('#availability span[class="available"]').length;
}

// SKUs
// Color
var sel = 'select.dropdown option';
Scraper.step(($(sel).length > 0), 'select', sel, getSku);


// Static basic data
Scraper.add('title', $('h1 .productName').text());
Scraper.add('id', $('#itemNumber').text());

Scraper.add('colors', $('.js_color li').length);
Scraper.add('sizes', $('.js_size li').length);
Scraper.add('Rating_Count', $('.description .rating .nrOfReviews').attr('content'));
Scraper.add('Rating_Value', $('.description .rating .p_rating100').attr('content'));
Scraper.add('recommendation', $('.recommendationIndex strong').text());
Scraper.add('instock', instock);
Scraper.add('skus_total', skus);
Scraper.add('available', prices.length);
Scraper.add('index', Scraper.avg(prices));

Scraper.finish();
