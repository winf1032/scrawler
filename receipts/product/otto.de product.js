/*global Scraper*/
"use strict";

/*
 Known interaction buttons
 js_color (click)
 js_size (click)
 js_height (fixed, click)
 js_temper (select)
 js_heatClass (select)
 js_version (select) // ignored (Garantie)
 js_mounting (select)
*/

// Initialize data types
var prices = [],
  skus = 0,
  instock = 0,
  sizes = [
      '.js_size option[selected]',
      '.js_size .selectedValue',
      '.js_temper .selectedValue',
      '.js_plaidSize .selectedValue',
    ];

// Applied to each SKU
function getSku() {
  var size = null;

  // Find size selector
  for (var i = 0; i < sizes.length; i++) {
    // Check if element exists
    if ($(sizes[i]).length) {
      // Get the size format
      size = $(sizes[i]).text();
      break;
    }
  }

  // Unknown size-case, break
  if (size === null) {
    Scraper.err('No size selector');
  }

  // Get price values
  var res = size.match(/[0-9]{1,4}/g),
    rPrice = parseFloat($("#reducedPriceAmount").text().replace(',', '.')),
    nPrice = parseFloat($("#normalPriceAmount").text().replace(',', '.')),
    price = rPrice ? rPrice : nPrice,
    n, l, b, d = 1;

  // Calculate the index
  if (Array.isArray(res)) {
    // Diameter
    if (res.length === 1) {
      d = parseInt(res[0]);
      prices.push(price / (Math.PI * (Math.pow(d, 2) / 4)));

      // Rectangular
    } else if (res.length === 2) {
      l = parseInt(res[0]);
      b = parseInt(res[1]);

      prices.push(price / (l * b));

      // Rectangular with two pieces
    } else if (res.length === 3) {
      n = parseInt(res[0]);
      l = parseInt(res[1]);
      b = parseInt(res[2]);

      prices.push((price / (l * b)) / n);

      // Not known yet
    } else {
      Scraper.err('Unknown size', size);
    }

    // Unknown size format
  } else {
    Scraper.err('Unknown size format', res);
  }

  // Add some more interesitng values
  skus += 1;
  instock = instock + $('#availability span[class="available"]').length;
}

// SKUs
// Color
var sel = '.js_color li:not(.p_disabled)';
Scraper.step(($(sel).length > 0), 'click', sel, function () {
  // Color select
  var sel = '.js_color option:not([disabled="disabled"])';
  Scraper.step(($(sel).length > 0), 'select', sel, function () {
    // Size
    var sel = '.js_size li:not(.p_disabled)';
    Scraper.step(($(sel).length > 0), 'click', sel, function () {
      // Size select
      var sel = '.js_size option:not([disabled="disabled"])';
      Scraper.step(($(sel).length > 0), 'select', sel, function () {
        // Height
        var sel = '.js_height li:not(.p_disabled)';
        Scraper.step(($(sel).length > 0), 'click', sel, function () {
          // Temper
          var sel = '.js_temper option:not([disabled="disabled"])';
          Scraper.step(($(sel).length > 0), 'select', sel, function () {
            // Heat Class
            var sel = '.js_heatClass option:not([disabled="disabled"])';
            Scraper.step(($(sel).length > 0), 'select', sel, function () {
              // Plaid Size
              var sel = '.js_plaidSize.dimProperty li:not(.p_disabled)';
              Scraper.step(($(sel).length > 0), 'click', sel, function () {
                // Pillow Size
                var sel = '.js_pillowSize.dimProperty li:not(.p_disabled)';
                Scraper.step(($(sel).length > 0), 'click', sel, function () {
                  // Material
                  var sel = '.js_material option:not([disabled="disabled"])';
                  Scraper.step(($(sel).length > 0), 'select', sel, function () {
                    // Mounting
                    var sel = '.js_mounting option:not([disabled="disabled"])';
                    Scraper.step(($(sel).length > 0), 'select', sel, getSku);
                  });
                });
              });
            });
          });
        });
      });
    });
  });
});

// Static basic data
Scraper.add('title', $('body h1').text());
Scraper.add('id', $('span[itemprop="productID"]').text());
Scraper.add('colors', $('.js_color li').length);
Scraper.add('sizes', $('.js_size li').length);
Scraper.add('Rating_Count', $('.description .rating .nrOfReviews').attr('content'));
Scraper.add('Rating_Value', $('.description .rating .p_rating100').attr('content'));
Scraper.add('recommendation', $('.recommendationIndex strong').text());
Scraper.add('instock', instock);
Scraper.add('skus_total', skus);
Scraper.add('index', Scraper.avg(prices));

Scraper.finish();
