"use strict";

// http://www.home24.de/esprit/teppich-swirl-160-x-230-cm
// \/\w{1,}\/

console.log('hallo?');
var prices = [],
  skus = 0,
  instock = 0,
  sizes = [
      'select#variations option[selected]',
      '.pdp-choosen-article p',
      'li.selected label',
      '.filter-options ul.radio-list li.selected label'
    ],
  marks = [
      /\d+[\scm]*x[\s]*\d+[\scm]*/,
      /ø*\d+\s*cm/
    ];

function getSku() {
  var size = null,
    res = [];


  var rPrice = parseFloat(jq214("span.pdp-new-price").text().replace('.', '').replace(',', '.')),
    nPrice = parseFloat(jq214(".price .current").text().replace('.', '').replace(',', '.')),
    price = rPrice ? rPrice : nPrice,
    l, b, d = 1;

  console.log(price);
  /*
  // Find size selector
  for (var i = 0; i < sizes.length; i++) {
    if (jq214(sizes[i]).length) {
      size = jq214(sizes[i]).text();
      break;
    }
  }
  if (size === null) {
    Scraper.err('No size selector');
  }


  // Find mark option
  for (i = 0; i < marks.length; i++) {
    var tmp = size.match(marks[i]);
    if (tmp) {
      res = tmp[0].match(/[0-9]{1,4}/g);
      break;
    }
  }
  if (res.length === 0) {
    Scraper.err('No mark selector');
  }

  // Diameter
  if (res.length === 1) {
    d = parseInt(res[0]);
    prices.push(price / (Math.PI * (Math.pow(d, 2) / 4)));
    // Rectangular
  } else if (res.length === 2) {
    l = parseInt(res[0]);
    b = parseInt(res[1]);

    prices.push(price / (l * b));
  }

  skus += 1;
  console.log('sku: ' + price);
  */
}

// SKUs
// Color
var sel = 'select#variations option';
var x = Scraper.step((jq214(sel).length > 0), 'select', sel, function () {
  // Color select
  var sel = 'ul.radio-list li div';
  return Scraper.step((jq214(sel).length > 0), 'click1', sel, getSku);
});

jq214.when(x).then(function (x) {
  console.log('###After SKUs###');
  // Static basic data
  Scraper.add('title', jq214('h1.pdp-name').text().trim());
  Scraper.add('id', jq214('.article-data .value[itemprop="sku"]').text());
  Scraper.add('colors', jq214('.pdp-related-configs a').length);
  Scraper.add('sizes', jq214('select#variations option').length);
  var counts = jq214('h1.pdp-reviews').text();
  Scraper.add('Rating_Count', counts.match(/\d+/)[0]);
  Scraper.add('Rating_Value', jq214('.pdp-star-rating strong').text());
  Scraper.add('skus_total', skus);
  Scraper.add('index', Scraper.avg(prices));


  Scraper.finish();
});
