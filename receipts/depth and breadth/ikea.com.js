"use strict";

// Initialize types of variables
var styles = [],
  articles = [];

// Get each product element
jq214('.productContainer').each(function () {
  var category = jq214(this).find('.prodDesc').text().trim().toLowerCase();

  // Focus on matching categories
  if ((category.search('matratze') >= 0) || category.search('boxspring') >= 0) {
    // Equal names without special characters
    var name = jq214(this).find('.prodName').text().trim()
      .replace(/[^a-z0-9\s]/gi, '');

    // Add article
    articles.push(name);

    // Ignore duplicate names for style count
    if (styles.indexOf(name) === -1) {
      styles.push(name);
    }
  }
});

// Save data
Scraper.add('styles', styles.length);
Scraper.add('colors', articles.length);

// Add links
Scraper.links('#pagination a', 'deep matratzen');

// Finish
Scraper.finish();
