"use strict";

// Initialize types of variables
var styles = [],
  articles = [];

// Get each product element
jq214('.article-tile__name').each(function () {
  // Focus on the names
  var name = jq214(this).text().trim();

  // Add article
  articles.push(name);

  // Ignore duplicate names for style count
  if (styles.indexOf(name) === -1) {
    styles.push(name);
  }
});

// Save data
Scraper.add('styles', styles.length);
Scraper.add('colors', articles.length);

// Add link
Scraper.links('.pagination a', 'deep');

// Finish
Scraper.finish();
