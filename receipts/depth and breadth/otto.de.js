"use strict";

var scrollspeed = 3000;

// Scroll down
jq214('html,body').animate({
  scrollTop: document.body.scrollHeight
}, scrollspeed);

// Second scroll down due to changed height
setTimeout(function () {
  jq214('html,body').animate({
    scrollTop: document.body.scrollHeight
  }, scrollspeed);
}, scrollspeed);


// Wait for previous scroll down actions
setTimeout(function () {
  // Select data
  var styles = jq214('#san_searchResult article.product').length;
  var colors = jq214('#san_searchResult li.colorThumb:not(.p_selected)').length;

  // Save data
  Scraper.add('styles', styles);
  Scraper.add('colors', colors);

  // Add links
  Scraper.links('.san_paging_block a', 'deep');

  // Finish
  Scraper.finish();
}, scrollspeed * 2 + 100);
