"use strict";

// Get the relevant fields
var styles = jq214('table .row-image a').length;
var colors = jq214('table tr .colorflags-selection:not(.selected)').length;

// Save data
Scraper.add('styles', styles);
Scraper.add('colors', colors);

// Finish
Scraper.finish();
