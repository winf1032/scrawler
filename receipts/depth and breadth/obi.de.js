"use strict";

// Initialize types of variables
var styles = [],
  articles = [];

// Get each product element
jq214('li.product').each(function () {
  // Focus on the first two words
  var name = jq214(this).find('.description a').text().trim()
    .split(/\s+/).slice(0, 2).join(' ');

  // Focus on the first four numbers
  var id = jq214(this).find('.checkbox input.comparebox')
    .val().slice(0, 4);

  // Add article
  articles.push(comb);

  // Build a style identifying string
  var comb = name + id;

  // Ignore duplicate names for style count
  if (styles.indexOf(comb) === -1) {
    styles.push(comb);
  }
});

// Save data
Scraper.add('styles', styles.length);
Scraper.add('colors', articles.length);

// Finish
Scraper.finish();
