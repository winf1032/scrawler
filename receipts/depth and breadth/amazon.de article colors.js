"use strict";

if (jq214('#result_0 .a-spacing-small a').length === 0) {
  Scraper.err('no first article');
} else {
  Scraper.links('#result_0 .a-spacing-small a', 'amazon article');
}

Scraper.finish();
