"use strict";

var colors = jq214('#variation_color_name li').length;

if (colors === 0) {
  Scraper.err('no colors');
} else {
  Scraper.add('colors', colors);
}

Scraper.finish();
