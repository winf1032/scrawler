"use strict";

// Add new link (only the next one)
Scraper.links('.pagnRA a', 'deep');

// Initialize types of variables
var styles = [],
  articles = [];

// Get each product element
jq214('.s-result-item').each(function () {
  // Ignore sponsored articles
  if (jq214(this).find('h5').length === 0) {
    // Basic data
    var asin = jq214(this).attr('data-asin');
    var name = jq214(this).find('h2').text().trim();
    var producer = jq214(this)
      .find('.a-spacing-mini .a-spacing-mini span:nth-child(2)').text().trim();
    var price = parseFloat(jq214(this)
      .find('.s-price').text().trim().slice(4).replace('.', '').replace(',', '.'));

    // Add relevant products
    articles.push({
      asin: asin,
      name: name,
      producer: producer,
      price: price
    });
  }
});

// Debug information
if (articles.length === 0) {
  Scraper.err('nothing found');
}

// Save data
Scraper.add('styles', articles.length);
Scraper.add('articles', articles);

// Finish
Scraper.finish();
