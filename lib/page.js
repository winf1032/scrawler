"use strict";

var _ = require('underscore');
var URI = require('URIjs');
var Q = require('q');
var siteDb = require('./db/siteDb');
var pageDb = require('./db/pageDb');
var dataDb = require('./db/dataDb');

var domain = null;
var counter = 0;
var types = {};

exports.normalize = function (url) {
  if (url) {
    // Avoid to get bitten by the URIjs bug
    if (!/^(f|ht)tps?:\/\//i.test(url)) {
      // Add protocol if not given
      url = "http://" + url;
    }

    // Enable easy comparison
    var uri = new URI(url).normalize();

    // Add 'www' as default subdomain
    if (!uri.subdomain()) {
      uri.subdomain('www');
    }

    return uri.toString();
  }
  return '';
};

exports.getURLPath = function (url) {
  url = this.normalize(url);
  var uri = new URI(url);

  // Avoid do get bitten by the URIjs bug
  if (!uri.protocol()) {
    uri.protocol('http');
    uri = new URI(uri.toString());
  }

  // Get full path including query and fragment
  return uri.resource();
};

exports.getDomain = function (url) {
  var uri = null;
  if (url) {
    uri = new URI(url);

    // Avoid do get bitten by the URIjs bug
    if (!uri.protocol()) {
      uri.protocol('http');
    }

    // Add 'www' as default subdomain
    if (!uri.subdomain()) {
      uri.subdomain('www');
    }

    // Get full hostname including subdomain
    url = uri.hostname();
  }

  return url;
};

exports.getStatus = function (status) {
  var code = 0;

  switch (status) {
  case 'complete':
    code = 5;
    break;
  case 'error':
    code = 4;
    break;
  case 'timeout':
    code = 3;
    break;
  default:
    code = 0;
    break;
  }

  return code;
};

exports.saveData = function (id, data) {
  var deferred = Q.defer();

  dataDb.addData(id, data).then(function (data) {
    deferred.resolve(data);
  }, function (err) {
    deferred.reject(new Error('Operating problem'));
  });

  return deferred.promise;
};

exports.savePages = function (pages) {
  // Async page handling
  var self = this,
    deferred = Q.defer(),
    data = [],
    promises = [];

  if ((pages.constructor === Array) && (pages.length > 0)) {
    promises = _.map(pages, function (page) {
      var promise = Q.defer();

      // Ignore wrong URLs and duplicates
      var url = self.normalize(page.url);
      if ((_.findWhere(data, {
          url: url,
          tag: page.tag
        }) === undefined)) {

        pageDb.addPage(
          url,
          _.has(page, 'status') ? page.status : null,
          _.has(page, 'depth') ? page.depth : 0,
          page.type,
          page.tag
        ).then(function (result) {
          data.push(result);
          promise.resolve();
        }, function (err) {
          promise.reject(err);
        });
      } else {
        // ignore duplicates
        promise.resolve(true);
      }

      return promise.promise;
    });


    // Complete block
    Q.all(promises).then(function () {
      deferred.resolve(data);
    }, function (err) {
      deferred.reject(new Error(err));
    });

  } else {
    deferred.reject(new Error('Wrong parameter'));
  }

  return deferred.promise;
};
