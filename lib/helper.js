"use strict";

exports.cl = console.log;

exports.js = function (obj) {
  return JSON.stringify(obj);
};

exports.jsonParse = function (str) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return false;
  }
};

exports.err = function (res, err) {
  res.status(500);
  res.json({
    status: 'error',
    err: err
  });
};
