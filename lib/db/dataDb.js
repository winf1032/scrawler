"use strict";

var Q = require('q');
var Data = require('../../models/data');
var pageDb = require('../db/pageDb');

exports.getData = function (query, populate) {
  var data = Data
    .find(query)
    .sort({
      created: -1 // Newest first
    });

  if (populate) {
    data = data.populate('page');
  }

  return data.exec();
};

exports.addData = function (id, data) {
  var deferred = Q.defer();

  var novice = new Data({
    page: id,
    data: data
  });

  novice.save(function (err) {
    if (err) deferred.reject(
      new Error('addData, novice.save(), err: ' + err)
    );

    pageDb.getPage({
      _id: id
    }, false).then(function (page) {
        page.data.unshift(novice._id);
        deferred.resolve(page.save());
      },
      function (err) {
        deferred.reject(
          new Error('addData, getPage(), err: ' + err)
        );
      });
  });

  return deferred.promise;
};
