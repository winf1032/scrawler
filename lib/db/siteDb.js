"use strict";

var _ = require('underscore');
var Q = require('q');
var URI = require('URIjs');
var Sites = require('../../models/site');
var page = require('../../lib/page');

exports.getSites = function (domain) {
  var query = {};
  if (domain) {
    query = {
      domain: domain
    };
  }

  return Sites.find(query).exec();
};

exports.getDistinct = function (field, query) {
  return Sites.distinct(field, query).exec();
};

exports.getSite = function (domain, type) {
  var query = {
    domain: domain,
    type: type
  };

  return Sites.findOne(query).exec();
};

exports.updateSite = function (domain, type, data) {
  var deferred = Q.defer();

  this.getSite(domain, type)
    .then(function (site) {
      if (site) {
        // Modify site object
        _.mapObject(data, function (val, key) {
          site[key] = val;
        });

        deferred.resolve(site.save());
      } else {
        //TODO Add Site
        deferred.reject(new Error('Site not found'));
      }
    }, function (err) {
      deferred.reject(new Error('Operating problem: ' + err));
    });

  return deferred.promise;
};

exports.addSite = function (site) {
  var self = this,
    deferred = Q.defer();

  // Convert milliseconds to seconds, from 5 minutes
  site.timeout = site.timeout > 300 ? site.timeout : site.timeout * 1000;

  this.getSite(site.domain, site.type)
    .then(function (obj) {
      // Update Site
      if (obj) {
        deferred.resolve(
          self.updateSite(site.domain, site.type, site)
        );
      } else {
        // Add a new site
        var novice = new Sites(site);

        deferred.resolve(novice.save());
      }
    }, function (err) {
      deferred.reject(new Error('Operating problem: ' + err));
    });

  return deferred.promise;
};

exports.deleteSite = function (id) {
  return Sites.findOneAndRemove({
    _id: id
  }).exec();
};
