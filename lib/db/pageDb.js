"use strict";

var _ = require('underscore');
var Q = require('q');
var URI = require('URIjs');
var Pages = require('../../models/page');

function getQuery(filter, type, tag, status) {
  var query = _.isObject(filter) ? filter : {};

  if (type) {
    query.type = {
      $in: Array.isArray(type) ? type : [type]
    };
  }

  if (tag) {
    query.tag = {
      $in: Array.isArray(tag) ? tag : [tag]
    };
  }

  if (status) {
    query.status = {
      $in: Array.isArray(status) ? status : [status]
    };
  }

  return query;
}

exports.getPage = function (filter, populate) {
  var query = _.isObject(filter) ? filter : {},
    page = Pages.findOne(query);

  if (populate) {
    page = page.populate('data');
  }

  return page.exec();
};


exports.getPages = function (populate, filter) {
  var query = getQuery({}, filter.type, filter.tag, filter.status);

  // Add-on filter by URL
  if (filter.url) {
    query.url = {
      $in: filter.url
    };
  }

  var pages = Pages.find(query, {
    data: {
      $slice: 1
    }
  });

  if (populate) {
    pages = pages.populate('data');
  }

  return pages.exec();
};


exports.update = function (filter, set) {
  var query = getQuery({}, filter.type, filter.tag, filter.status);

  return Pages.update(query, {
    $set: set
  }, {
    multi: true
  }).exec();
};

exports.delete = function (filter) {
  var query = getQuery({}, filter.type, filter.tag, filter.status);

  return Pages.remove(query, true).exec();
};

exports.updatePage = function (id, data) {
  var deferred = Q.defer();

  this.getPage({
    _id: id
  }, false).then(function (page) {
    if (page) {
      // Modify page object
      _.mapObject(data, function (val, key) {
        page[key] = val;
      });

      deferred.resolve(page.save());
    } else {
      deferred.reject(new Error('Page not found'));
    }
  }, function (err) {
    deferred.reject(new Error('Operating problem: ' + err));
  });

  return deferred.promise;
};

exports.addPage = function (url, status, depth, type, tag) {
  var self = this,
    deferred = Q.defer();

  this.getPage({
    url: url,
    tag: tag
  }, false).then(function (page) {
    if (page) {
      // Modify page
      page.count = page.count + 1;
      page.depth = depth < page.depth ? depth : page.depth;
      page.status = (status === null) ? page.status : status;
      page.type = type;

      deferred.resolve(page.save());
    } else {
      // Add a new page
      var novice = new Pages({
        url: url,
        status: (status === null) ? 1 : status,
        count: 1,
        depth: depth,
        type: type,
        tag: tag
      });

      deferred.resolve(novice.save());
    }
  }, function (err) {
    deferred.reject(new Error('Operating problem: ' + err));
  });

  return deferred.promise;
};

exports.getCount = function () {
  return Pages.aggregate({
    $group: {
      _id: '$status',
      total: {
        $sum: 1
      }
    }
  }).exec();
};

exports.getDistinct = function (field, query) {
  return Pages.distinct(field, query).exec();
};

exports.getNextPage = function (page) {
  //TODO max active time
  var query = {};

  // Only pending pages
  query.status = 1;

  // Add domain filter
  if (page.url) {
    query.url = {
      $regex: new RegExp(page.url)
    };
  }

  // Add page type
  if (page.type) {
    query.type = page.type;
  }

  // Add page type
  if (page.tag) {
    query.tag = page.tag;
  }

  // Add max depth
  if (page.depth) {
    query.depth = {
      '$lte': page.depth
    };
  }

  return this.getPage(query, false);
};
