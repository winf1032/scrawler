"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DataSchema = new Schema({
  page: {
    type: Schema.Types.ObjectId,
    ref: 'Page'
  },
  created: {
    type: Date,
    default: Date.now
  },
  data: Schema.Types.Mixed
});

module.exports = mongoose.model('Data', DataSchema);
