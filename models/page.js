"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PageSchema = new Schema({
  url: String,
  /* Status types
     0: ignored
     1: pending
     2: active
     3: timeout
     4: error
     5: finished */
  status: {
    type: Number,
    min: 0,
    max: 5
  },
  count: Number,
  depth: Number,
  type: String,
  data: [{
    type: Schema.Types.ObjectId,
    ref: 'Data'
  }],
  tag: String,
  created: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Page', PageSchema);
