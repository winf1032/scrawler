"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SiteSchema = new Schema({
  type: String,
  domain: String,
  timeout: {
    type: Number,
    default: 10000
  },
  function: String,
  created: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Site', SiteSchema);
